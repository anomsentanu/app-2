// File with top-level statements (Incorrect)
using System;

Console.WriteLine("Hello, world!");

// Move top-level statements into a method or class (Correct)
using System;

namespace app-2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, world!");
        }
    }
}
